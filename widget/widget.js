var widget = {
  modal: function(options) {
    var _this = this;

    var box = document.createElement('div');
    box.id = "modal-widget";

  },
  ///////////////////////////////////////////////////
  ///bar, from last task
  bar: function(options) {
    var _this = this;

    var template = '<div id="widget-icon"><a href="index.html" alt="Icon"><img src="http://icons.iconarchive.com/icons/hopstarter/sleek-xp-software/256/Yahoo-Messenger-icon.png"></a></div><div id="widget-close"><a href="#">&times;</a></div><div id="widget-body"><p id="widget-message"></p><input type="email" id="widget-user-email" placeholder="Email"><a href="#" id="widget-ok">Subscribe</a></div>';

    //create widget
    var box = document.createElement('div');
    document.body.appendChild(box);
    box.id = "widget-box";

    //hide box
    this.hide = function() {
      var position = this.position ? this.position : 'top';
      var tOut = setInterval(function() {
        box.style[position] = parseInt(box.style[position], 10) - 5 + "px";
        if( box.style[position] === "-50px" ) {
          clearInterval(tOut);
        }
      }, 10);
    };

    //close widget
    this.closeBox = function(reason) {
      if( reason == 'confirmed' ) {
        console.log('confirmed')
        var email = document.getElementById('widget-user-email').value;
        if( email.length > 5 ) {
          var reg = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
          var valid = reg.test(email);
          if( valid == true ) {
            var event = new CustomEvent('WidgetConfirmed', { 'detail': email });
            box.dispatchEvent(event);
            _this.clickOk();
            _this.hide();
          } else {
            alert('zły email');
          }
        }
      } else {
        console.log('user closed');
        if( typeof _this.close == 'function' ) {
          _this.close();
        }
        _this.hide();
      }
    };

    //user confirmed
    this.confirm = function() {
      _this.closeBox('confirmed');
    };

    for( key in options ) {
      this[key] = options[key];
    }

    if( this.position === 'bottom' ) {
      box.style.bottom = 0;
    } else {
      box.style.top = 0;
    }

    //check theme
    if( this.theme && this.theme === 'dark' ) {
      box.className = 'dark';
    } else {
      box.className = 'light';
    }

    //append content to widget
    box.innerHTML = template;

    var messageBox = document.getElementById('widget-message');
    messageBox.innerHTML = this.message;

    // click ok
    document.getElementById('widget-ok').onclick = this.confirm;

    //click close
    document.getElementById('widget-close').onclick = this.closeBox;
  }
};