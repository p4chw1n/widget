#Subscribe Widget
Fixed positioned newsletter widget

###Usage

First you need to include widget.js and widget.css file

```
#!html
<link rel="stylesheet" href="widget/widget.css">
<script src="widget/widget.js"></script>

```

Then create new instance of widget.bar

```
#!javascript

    var myBar = new widget.bar({
          message: 'Your message',
          close: function(){alert('close function')},
          clickOk: function(){alert('ok function')},
          position: 'top',
          theme: 'dark'
        });

```
This widget will dispatch event 'WidgetConfirmed' with user email which you can handle like this:

```
#!javascript

    document.addEventListener('WidgetConfirmed', function(email) {
      // your logic here
    });

```

### Options

#### message: (required)
A message which you want to display. It appears on center of widget box.

```
#!javascript
message: 'Your message'
```
#### clickOk: (required)
Function which will be triggered after click on 'Subscribe' button
```
#!javascript
close: function() {
  alert('sent!');
}
```

#### close: (optional)
Default: none
Function which you want to trigger on closing widget
```
#!javascript
close: function() {
  alert('closed!');
}
```
#### position: (optional)
Default: 'top'
Desired box position. Working options 'top' and 'bottom'. Any other value will evaluate to 'top'.
```
#!javascript
position: 'bottom'
```

####theme: (optional)
Default: 'light'
Theme for bar. Available 'light' and 'dark'
```
#!javascript
theme: 'dark'
```